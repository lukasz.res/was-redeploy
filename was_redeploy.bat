@echo off
REM to run script you have to pass project path
setlocal EnableDelayedExpansion
set "startTime=%time: =0%"

set PROJECT_PATH= %1

set WSADMIN_PATH="C:\Program Files (x86)\IBM\WebSphere\AppServer\bin"

if "%2" == "install" (
 goto checkWSAdmin
)

:runMaven
cd %PROJECT_PATH%
call mvn clean package
if not "%ERRORLEVEL%" == "0" (
	echo  - [[101;93m Maven exit Code = %ERRORLEVEL% [0m]
	goto end
)

:checkWSAdmin
echo - [[34mChecking WSAdmin[0m]...
cd %WSADMIN_PATH%
set SCRIPT=%~dp0%skrypt.py
if not exist wsadmin.bat (
	echo [101;93m  - WSAdmin not found at %WSADMIN_PATH% path [0m
	goto end
)
echo - [[34mWSAdmin exists[0m  ]

:runWSAdmin
echo - [[34mRunning WSAdmin[0m ]...
call wsadmin -lang jython -f %SCRIPT% %PROJECT_PATH%
if not "%ERRORLEVEL%" == "0" (
	goto end
)
set "endTime=%time: =0%"
goto showElapsedTime
goto EOF

:end
pause
goto EOF

:showElapsedTime
rem Get elapsed time:
set "end=!endTime:%time:~8,1%=%%100)*100+1!"  &  set "start=!startTime:%time:~8,1%=%%100)*100+1!"
set /A "elap=((((10!end:%time:~2,1%=%%100)*60+1!%%100)-((((10!start:%time:~2,1%=%%100)*60+1!%%100)"

rem Convert elapsed time to HH:MM:SS:CC format:
set /A "cc=elap%%100+100,elap/=100,ss=elap%%60+100,elap/=60,mm=elap%%60+100,hh=elap/60+100"

echo [32m Start:[0m    %startTime%
echo [32m End:[0m      %endTime%
echo [32m Elapsed:[0m  %hh:~1%%time:~2,1%%mm:~1%%time:~2,1%%ss:~1%%time:~8,1%%cc:~1%
