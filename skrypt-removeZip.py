import string, sys, re, os
from shutil import copyfile
from java.io import File, FileWriter
from javax.xml.parsers import DocumentBuilderFactory
from javax.xml.transform.dom import DOMSource
from javax.xml.transform.stream import StreamResult
from javax.xml.transform import TransformerFactory

projectPath = 'C:/workspace/arab-bank-bo/'
earPath = 'ab-bo-ear/'
pomXmlPath = projectPath + earPath + 'pom.xml'
pomXmlPathTemp = pomXmlPath + '-temp'

def removeZip():
	file = File(pomXmlPath)
	doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(file)
	doc.getDocumentElement().normalize()
	version = ''
	nList = doc.getElementsByTagName("plugin")
	for i in range(nList.getLength()):
		nNode = nList.item(i)
		artifactIdNodeList = nNode.getElementsByTagName("artifactId")
		for j in range(artifactIdNodeList.getLength()):
			artifactIdNode = artifactIdNodeList.item(j)
			if artifactIdNode.getTextContent() == 'maven-assembly-plugin':
				toRemoveNode =  artifactIdNode.getParentNode()
				toRemoveNode.getParentNode().removeChild(toRemoveNode)
	
	copyfile(pomXmlPath, pomXmlPathTemp)
	
	
	# write the content into xml file
	source = DOMSource(doc);
	newFile = File(pomXmlPath)
	writer = FileWriter(newFile);
	result = StreamResult(writer);

	transformerFactory = TransformerFactory.newInstance();
	transformer = transformerFactory.newTransformer();
	transformer.transform(source, result);	
#endDef

def restorePom():
	if os.path.exists(pomXmlPathTemp):
		copyfile(pomXmlPathTemp, pomXmlPath)
		os.remove(pomXmlPathTemp)
#endDef

def main():
	arg = sys.argv[1]
	if arg == 'removeZip':
		removeZip()
	elif arg == 'restorePom':
		restorePom()
main()
	
