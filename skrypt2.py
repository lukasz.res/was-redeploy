###########################################################################################################################################
#Purpose: This script will install, update a given application. It required a configuration file with necessary parameters
#         1st argument: Nature of Task (either 'install' or 'update' )
#         2nd argument: Configuration file absolute path (Eg: '/usr/local/applen/scripts/deploy/od_deploy.cfg'
#
#NOTE: Please note that all variables in the input configuration file are 'global'. Also, indent is important in any jython script
#
#Created: 13/02/2009
#Authors: Satish Kondamuri
#
#Change History:
#            --> Declared all variables as global in configuration file, otherwise the command 'execfile' not making the variables in configuration file 
#                availabe to this script (02/14/2009)
#
#
###########################################################################################################################################

import sys

# This function will only check if the given application exists in the configuration repository. Required argument is the application name
def AppExists(app_name):
    if (len(app_name) == 0):
          app_exist = 1
          return app_exist
    application = AdminConfig.getid("/Deployment:"+app_name+"/")
    if (len(application) == 0):
          app_exist = 1
    else:
          app_exist = 0
    return app_exist
#endDef
    
# This function will uninstall a given application and then install. 
def Install():
   return_code = AppExists(APP_NAME)
   if return_code == 0:
       print "\n Found application: ",APP_NAME , ". Uninstalling the application "
       AdminApp.uninstall(APP_NAME)
       AdminConfig.save()
       #Time delay required to update the config files
       #sleep(10)
       return_code_1 = AppExists(APP_NAME)
       if return_code_1 == 1:
           print "\n Successfully Uninstalled Application: ",APP_NAME
       else:
           print "\n *** ERROR *** : Uninstallation of application ", APP_NAME, " FAILED "
           sys.exit(3)
   else:
      print "\n +++ WARNING +++ : Unable to find the application ", APP_NAME, " in the repository. Installing the given application for first time"
      
   print "\n Starting installation of application: ", APP_NAME
   print AdminApp.install(EarFile,options_install)
   AdminConfig.save()
   return_code = AppExists(APP_NAME)
   if return_code == 0:
       print "\n +++ Success +++ : Successfully installed application '",APP_NAME, "'"
   else:
       print "\n *** ERROR *** : Installation of application '", APP_NAME, "' FAILED "
       sys.exit(3)
#endDef


# This function will update the given application. Required options are the application name, content type and application options  
def Update():
    return_code = AppExists(APP_NAME)
    if return_code == 0:
         print "\n Found application '",APP_NAME , "'. Updating the application "
         print AdminApp.update(APP_NAME, CONTENT_TYPE, options_update)
         AdminConfig.save() 
         #RC = AdminApp.update(APP_NAME, CONTENT_TYPE, options_update)
         #if RC == 0:
         print "\n +++ Success +++ : Successfully updated application '",APP_NAME,"'"
         #else:
         #   print "\n *** ERROR *** : Updation of application '", APP_NAME, "' FAILED [ RC = ",RC, " ]"
         #   sys.exit(4)
    else: 
         print "\n *** ERROR *** : Unable to find the application '", APP_NAME ,"' in the repository. Please Verify "
         sys.exit(2)
#endDef   

# This is the main routine that calls all other routines as per the input argument
def main():
   global task
   global configfile

   if (len(sys.argv) != 2):
        print "\n *** ERROR *** : No enough input arguments passed. Nature of task, and absolute path of configuration file needed. ARGS = ",len(sys.argv)
        sys.exit(1)
 
   task = sys.argv[0]
   configfile = sys.argv[1]

   if (len(configfile) == 0):
      print "\n *** ERROR *** : No configuration file passed. Please Verify if configuration file with necessary parameters is passed ",configfile
      sys.exit(1)
   #endIf
 
   execfile(configfile)

   if task == 'install':
      Install()
   elif task == 'update':
      Update()
   else:
      print "\n *** ERROR ***: Wrong option passed. Please Verify [option=", task, "]"
      sys.exit(1)
   
#endDef 

#######################################################################################################################
##########################         This is the first line of the script that is exececuted      #######################
#######################################################################################################################
main()