import string, sys, re

class Application:
	#webApplicationName - could be taken from Websphere
	def __init__(self, applicationName, webApplicationName, projectPath, earPath):
		self.applicationName = applicationName
		self.webApplicationName = webApplicationName
		self.projectPath = projectPath
		self.earPath = earPath
		#----------------------------------------------------------------------------
		# If both versions are different, version should be taken from pom.xml
		# If both versions are the same, version should be also taken from pom.xml
		#----------------------------------------------------------------------------
		self.version = readVersion(projectPath)
		self.param = ''
		if self.applicationName == 'arab-bank-bo':
			self.param = '[ "Finanteq Mobile Banking Arab Bank Back Office" ' + self.webApplicationName + self.version + '.war,WEB-INF/web.xml WebSphere:cell=lreszka-1Node01Cell,node=lreszka-1Node01,server=server1 ]] -MapWebModToVH [[ "Finanteq Mobile Banking Arab Bank Back Office" ' + self.webApplicationName + self.version + '.war,WEB-INF/web.xml default_host ]'
		elif self.applicationName == 'arab-bank-ear':
			self.param = '[ arab-bank-dao-' + self.version + '.jar arab-bank-dao-' + self.version + '.jar,META-INF/ejb-jar.xml WebSphere:cell=lreszka-1Node01Cell,node=lreszka-1Node01,server=server1 ][ arab-bank-web-' + self.version + '.war arab-bank-web-' + self.version + '.war,WEB-INF/web.xml WebSphere:cell=lreszka-1Node01Cell,node=lreszka-1Node01,server=server1 ]] -MapWebModToVH [[ arab-bank-web-' + self.version + '.war arab-bank-web-' + self.version + '.war,WEB-INF/web.xml default_host ]'

def readVersion(projectPath):
	pomXmlPath = projectPath + 'pom.xml'
	regexp = re.compile(r'^(.*<version>)(?P<version>.*)(</version>$)')

	f = open(pomXmlPath, "r")
	x = str(f.readline())
	while not regexp.search(x):
		x = str(f.readline())
	m = re.match(regexp, x)
	return m.group('version')
#endDef


def readVersionFromServer(application):
	arabBankBo = AdminApp.listModules(application.applicationName, '-server')
	p = re.compile(application.webApplicationName + '.*.war')
	m = p.findall(arabBankBo)
	if len(m)==0:
		raise Exception('Cannot find ab-bo-web in AdminApp.listModules')
	m = m[0]
	m = m.replace(application.webApplicationName, '')
	m = m.replace('.war', '')
	return m
#enDef

def main():
	project_path =  sys.argv[0].replace('\\', '/') + '/'
	application = Application('arab-bank-bo', 'ab-bo-web-', project_path, 'ab-bo-ear/target/ab-bo.ear')
	if project_path.find("bo") != -1 :
		application = Application('arab-bank-bo', 'ab-bo-web-', project_path, 'ab-bo-ear/target/ab-bo.ear')
	if  project_path.find("adapter") != -1:
		earFileName = "arab-bank-ear/target/arab-bank-ear-" + readVersion(project_path) + ".ear"
		application = Application('arab-bank-ear', 'arab-bank-web-', project_path, earFileName)

	command = '[ -operation update -contents ' + application.projectPath + application.earPath + ' -nopreCompileJSPs -installed.ear.destination $(APP_INSTALL_ROOT)/lreszka-1Node01Cell -distributeApp -nouseMetaDataFromBinary -createMBeansForResources -noreloadEnabled -nodeployws -validateinstall warn -noprocessEmbeddedConfig -filepermission .*\.dll=755#.*\.so=755#.*\.a=755#.*\.sl=755 -noallowDispatchRemoteInclude -noallowServiceRemoteInclude -asyncRequestDispatchType DISABLED -nouseAutoLink -noenableClientModule -clientMode isolated -novalidateSchema -MapModulesToServers [' + application.param + ']]'
	try:
		AdminApp.update(application.applicationName, 'app',  command)
		AdminConfig.save()
		print "Updated successfull."
	except:
		print sys.exc_info()[0]
		os.exit(1)
#endDef

main()

